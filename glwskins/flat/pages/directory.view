
onEvent(videoInfo, {
  $clone.itemData <- $event;
  $clone.itemMenu = "video";
});

onEvent(pluginInfo, {
  $clone.itemData <- $event;
  $clone.itemMenu = "plugin";
});

onEvent(defaultInfo, {
  $clone.itemData <- $event;
  $clone.itemMenu = "default";
});


style(sidebarBackdrop, {
  color: 0;
  alpha: 0.3;
});

onEvent(itemMenu, { $clone.itemMenu = void; }, $clone.itemMenu);
onEvent(back,     { $clone.itemMenu = void; }, $clone.itemMenu);
onEvent(cancel,   { $clone.itemMenu = void; }, $clone.itemMenu);
onEvent(menu,     { toggle($clone.showSidebar); },
       isVoid($clone.itemMenu));

onEvent(ChangeView, deliverEvent($clone.settings.options, "Cycle"));

multiopt($view.path,
	 $clone.settings,
	 _("Page layout"),
	 $self.persistent.glwUserView,
	 // List of view follows
	 // First custom view set by model
	 makeUri(_("Custom"), $self.model.metadata.glwview),

	 // Optionally a list of custom views set by model
	 vectorize($self.model.metadata.glwviews),

	 // Then comes external views selected based on content
	 translate($self.model.contents, void,
		   "tracks",     vectorize($core.glw.views.standard.tracks),
		   "album",      vectorize($core.glw.views.standard.album),
		   "albums",     vectorize($core.glw.views.standard.albums),
		   "artist",     vectorize($core.glw.views.standard.artist),
//		   "tvchannels", vectorize($core.glw.views.standard.tvchannels),
		   "images",     vectorize($core.glw.views.standard.images),
		   "movies",     vectorize($core.glw.views.standard.movies)
		    ),

	 // Then comes internal views selected based on content
	 translate($self.model.contents, void,
		   "album",      makeUri(_("Album"),      "album.view"),
		   "artist",     makeUri(_("Artist"),     "artist.view"),
		   "images",     makeUri(_("Grid"),     "grid.view"),
		   "grid",       makeUri(_("Grid"),     "grid.view"),
		   "searchresults", makeUri(_("Search results"), "searchresults.view")),

	 select($self.model.safeui, void,
		vectorize($core.glw.views.standard.directory)),
	 makeUri(_("List"), "list.view"),
	 select($self.model.type == "settings", void, makeUri(_("Grid"), "grid.view"))
	);

$view.overlay = translate($clone.itemMenu,
                          select($clone.showSidebar, "sidebar", void),
                          "video", "fullscreen",
                          "default", "popup",
                          "plugin", "fullscreen");


widget(container_z, {

  widget(layer, {

    style: "PageContainer";

    widget(clip, {

      rightPx: select($clone.showSidebar, 22em, 0);
      blurOutside: 1;
      alphaOutside: 0.05;


      widget(container_x, {


        widget(expander_x, {
          expansion: iir($view.showNavbar, 4);
          alpha: iir($view.showNavbar, 4);

          widget(container_y, {
            width: 4em;
            align: center;

            widget(icon, {
              align: right;
              source: "skin://icons/ic_expand_less_48px.svg";
              size: 2em;
              alpha: 0.5;
            });

            widget(container_x, {

              widget(icon, {
                align: center;
                source: "skin://icons/ic_chevron_left_48px.svg";
                size: 2em;
                alpha: 0.5;
              });

              widget(icon, {
                id: "navbardot";
                focusable: true;
                align: center;
                source: "skin://icons/dot.png";
                size: 2em;
                alpha: 0.4 + isFocused();
                color: sinewave(1) * 0.25 + 0.75;
                onEvent(right, {
                  $view.showNavbar = false;
                  focus("main");
                });

                onEvent(left, {
                  $self.close = true;
                });

                onEvent(up, targetedEvent("scrollbar", up));
                onEvent(down, targetedEvent("scrollbar", down));
              });
            });

            widget(icon, {
              align: right;
              source: "skin://icons/ic_expand_more_48px.svg";
              alpha: 0.5;
              size: 2em;
            });
          });
        });

        widget(loader, {

          alpha: iir(translate($view.overlay, 1,
                               "sidebar", 0.3,
                               "popup", 0.3,
                               "fullscreen", 0), 4);

          onEvent(right, {
            $clone.showSidebar = true;
          }, true, false);

          onEvent(left, {
            $view.showNavbar = true;
            focus("navbardot");
          }, true, false);

          id: "main";
          filterConstraintX: true;
          noInitialTransform: true;
          time: 0.05;
          source: $view.path;
        });
      });
    });

    widget(loader, {
      hidden: !$clone.showSidebar;
      filterConstraintX: true;
      source: "skin://menu/sidebar.view";
    });

    widget(loader, {
      // Hide if itemMenu is 0 or if we shouldn't show it at all
      hidden: isVoid($clone.itemMenu);
      time: 0;
      noInitialTransform: true;
      source: "skin://ctxmenu/" + $clone.itemMenu + "_details.view";
        .args <- $clone.itemData;
    });
  });


  widget(container_y, {

    alpha: iir($ui.showTopIcons && isVoid($clone.itemMenu), 4);

    widget(container_x, {
      space(1);
      padding: [1em, 0];
      height: 3em;
      widget(icon, {
        clickable: isVoid($clone.itemMenu);
        angle: iir($clone.showSidebar, 4) * -90;

        onEvent(activate, {
          toggle($clone.showSidebar);
        });

        navFocusable: false;
        color: 0.5 + iir(isHovered(), 4);
        size: 2em;
        source: "skin://icons/ic_menu_48px.svg";
      });
    });
  });

});
